import React, { Component } from 'react';
import axios from 'axios';
import Book from './Book';

class User extends Component {

    constructor(){
        super();
        this.state = {
            registration: 'pending',
            login: 'pending',
            userInformation: {
                email: '',
                nameUser: '',
                surnameUser: '',
                avatar: ''
            }
        }
    }

    componentWillMount(){

    }

    addUser(e){
        let that = this;
        e.preventDefault();
        axios.post('/api/auth', {
            userEmail: this.refs.email.value,
            userPassword: this.refs.password.value,
            userName: this.refs.name.value,
            userSurname: this.refs.surname.value
        })
            .then(function (response) {
              console.log(response);
                that.setState({registration : response.data.message})
            })
            .catch(function (err) {
                that.setState({registration : 'False'})
            });
    }

    loginUser(e){
        let that = this;
        e.preventDefault();
        axios.post('/api/login', {
            userEmail: this.refs.emailLogin.value,
            userPassword: this.refs.passwordLogin.value
        })
            .then(function (response) {
                that.setState({login : 'Ok'})
            })
            .catch(function (error) {
                that.setState({login : 'False'})
            });
    }

    render() {
        return (
            <div>
                <h1>User Page</h1>
                <h3>Registration</h3>
                <form ref="registration">
                    <input type="text" ref="email" placeholder="email"/>
                    <input type="text" ref="password" placeholder="password"/>
                    <input type="text" ref="name" placeholder="name"/>
                    <input type="text" ref="surname" placeholder="surname"/>
                    <button onClick={this.addUser.bind(this)}>Register</button>
                    <p>{this.state.registration}</p>
                </form>
                <h3>Sign In</h3>
                <form ref="signIn">
                    <input type="text" ref="emailLogin" placeholder="email"/>
                    <input type="text" ref="passwordLogin" placeholder="password"/>
                    <button onClick={this.loginUser.bind(this)}>Login</button>
                    <p>{this.state.login}</p>
                </form>
                {/*<Book/>*/}
            </div>
        );
    }
}

export default User;
