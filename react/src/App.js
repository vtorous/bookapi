import React, { Component } from 'react';
import User from './User';
import { Route } from 'react-router-dom';

class App extends Component {



      render() {
        return (
          <div className="App">
              <Route exact path='/' component={User}/>
          </div>
        );
      }
}

export default App;
