// import React, { Component } from 'react';
// import axios from 'axios';
//
//
// class Book extends Component {
//
//
//     addBook(e){
//         e.preventDefault();
//         axios.post('/api/new-book', {
//             nameBook: this.refs.nameBook.value,
//             numberOfPage: this.refs.numberOfPage.value,
//             ISBN: this.refs.ISBN.value,
//             language: this.refs.language.value,
//             publisher: this.refs.publisher.value,
//             description: this.refs.description.value,
//             yearOfPublished: this.refs.yearOfPublished.value
//         })
//             .then(function (response) {
//                 console.log(response);
//             })
//             .catch(function (error) {
//                 console.log(error);
//             });
//     }
//     render() {
//         return (
//             <div>
//                 <h1>Book Page</h1>
//                 <h3>Add book</h3>
//                 <form ref="addBook">
//                     <input type="text" ref="nameBook" placeholder="book title"/>
//                     <input type="text" ref="numberOfPage" placeholder="number of page"/>
//                     <input type="text" ref="ISBN" placeholder="ISBN"/>
//                     <input type="text" ref="language" placeholder="language"/>
//                     <input type="text" ref="publisher" placeholder="publisher"/>
//                     <input type="text" ref="description" placeholder="description"/>
//                     <input type="text" ref="yearOfPublished" placeholder="year of published"/>
//                     <button onClick={this.addBook.bind(this)}>Add Book</button>
//                 </form>
//                 <h3>Edit book</h3>
//                 {/*<form ref="signIn">*/}
//                     {/*<input type="text" ref="emailLogin" placeholder="email"/>*/}
//                     {/*<input type="text" ref="passwordLogin" placeholder="password"/>*/}
//                     {/*<button>Login</button>*/}
//                 {/*</form>*/}
//             </div>
//         );
//     }
// }
//
// export default Book;
