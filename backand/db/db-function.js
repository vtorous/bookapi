var pgp = require('pg-promise')(/*options*/);

const cn = {
  host: 'localhost',
  port: 5432,
  database: 'library',
  user: 'postgres',
  password: 'root'
};


function connectDb() {
  return pgp(cn);
}

module.exports = {
  connectDb: connectDb,
};