
INSERT INTO public.authors VALUES (1, 'Taras', 'Shevchenko');
INSERT INTO public.authors VALUES (2, 'Daniel', 'Defoe');
INSERT INTO public.authors VALUES (3, 'Louisa', 'May Alcott');
INSERT INTO public.authors VALUES (4, 'Jonathan', 'Swift');
INSERT INTO public.authors VALUES (5, 'Taras', 'Shevchenko');
INSERT INTO public.authors VALUES (6, 'Thomas', 'Hardy ');
INSERT INTO public.authors VALUES (7, 'Jack', 'London');
INSERT INTO public.authors VALUES (8, 'Max', 'Beerbohm');
INSERT INTO public.authors VALUES (9, 'Edith', 'Wharton');
INSERT INTO public.authors VALUES (10, 'Sylvia', 'Townsend Warner');
INSERT INTO public.authors VALUES (11, 'George', 'Orwell');
INSERT INTO public.authors VALUES (12, 'Salman', 'Rushdie');


INSERT INTO public.publishers VALUES (1, 'Green Frog Publishing');
INSERT INTO public.publishers VALUES (2, 'Brick Cave Media');
INSERT INTO public.publishers VALUES (3, 'Opportunity Publishing');
INSERT INTO public.publishers VALUES (4, 'Korero Press');
INSERT INTO public.publishers VALUES (5, 'Arma Technologies');
INSERT INTO public.publishers VALUES (6, 'eLectio Publishing');
INSERT INTO public.publishers VALUES (7, 'Alfresco Press');
INSERT INTO public.publishers VALUES (8, 'Virgogray Press');
INSERT INTO public.publishers VALUES (9, 'Pants On Fire Press');
INSERT INTO public.publishers VALUES (10, 'Wordpool Press');
INSERT INTO public.publishers VALUES (11, 'ZitrO Publications');
INSERT INTO public.publishers VALUES (12, 'Wolfpack Publishing');
INSERT INTO public.publishers VALUES (13, 'Redd Ink Press');

INSERT INTO public.languages VALUES (1, 'english');
INSERT INTO public.languages VALUES (2, 'france');
INSERT INTO public.languages VALUES (3, 'spanish');
INSERT INTO public.languages VALUES (4, 'china');
INSERT INTO public.languages VALUES (5, 'ukrainian');
INSERT INTO public.languages VALUES (6, 'poska');
INSERT INTO public.languages VALUES (7, 'swiden');


INSERT INTO public.genres VALUES (1, 'Science fiction');
INSERT INTO public.genres VALUES (2, 'Satire');
INSERT INTO public.genres VALUES (3, 'Drama');
INSERT INTO public.genres VALUES (4, 'Action and Adventure');
INSERT INTO public.genres VALUES (5, 'Romance');
INSERT INTO public.genres VALUES (6, 'Mystery');
INSERT INTO public.genres VALUES (7, 'Horror');
INSERT INTO public.genres VALUES (8, 'Self help');
INSERT INTO public.genres VALUES (9, 'Health');
INSERT INTO public.genres VALUES (10, 'Guide');
INSERT INTO public.genres VALUES (11, 'Travel');
INSERT INTO public.genres VALUES (12, 'Childrens');
INSERT INTO public.genres VALUES (13, 'Religion, Spirituality');
INSERT INTO public.genres VALUES (14, 'Science');
INSERT INTO public.genres VALUES (15, 'History');
INSERT INTO public.genres VALUES (16, 'Math');
INSERT INTO public.genres VALUES (17, 'Anthology');
INSERT INTO public.genres VALUES (18, 'Poetry');
INSERT INTO public.genres VALUES (19, 'Encyclopedias');
INSERT INTO public.genres VALUES (20, 'Dictionaries');
INSERT INTO public.genres VALUES (21, 'Comics');
INSERT INTO public.genres VALUES (22, 'Art');
INSERT INTO public.genres VALUES (23, 'Cookbooks');
INSERT INTO public.genres VALUES (24, 'Diaries');
INSERT INTO public.genres VALUES (25, 'Journals');
INSERT INTO public.genres VALUES (26, 'Prayer books');
INSERT INTO public.genres VALUES (27, 'Series');
INSERT INTO public.genres VALUES (28, 'Trilogy');
INSERT INTO public.genres VALUES (29, 'Biographies');
INSERT INTO public.genres VALUES (30, 'Autobiographies');
INSERT INTO public.genres VALUES (31, 'Fantasy');



INSERT INTO public.books VALUES (1, 'Robinson Crusoe', 250, '978-039323942', 1, 2, ' Robinson Crusoe is a novel by Daniel Defoe, first published on 25 April 1719. The first edition credited the work protagonist Robinson Crusoe as its author, leading many readers to believe he was a real person and the book a travelogue of true incidents. Epistolary, confessional, and didactic in form, the book is presented as an autobiography of the title character (whose birth name is Robinson Kreutznaer)—a castaway who spends twenty-eight years on a remote tropical desert island near Trinidad, encountering cannibals, captives, and mutineers, before ultimately being rescued. The story has since been thought to be based on the life of Alexander Selkirk, a Scottish castaway who lived for four years on a Pacific island called \"Más a Tierra", now part of Chile, which was renamed Robinson Crusoe Island in 1966, but various literary sources have also been suggested.
						  ', 2, 1996, NULL);
INSERT INTO public.books VALUES (2, 'The Reluctant Rebel', 180, '978-039323942', 5, 3, 'Masterfully fulfilled by Peter Fedynsky, Voice of America journalist and expert on Ukrainian studies, this first ever English translation of the complete Kobzar brings out Ukraine''s rich cultural heritage. As a foundational text, The Kobzar has played an important role in galvanizing the Ukrainian identity and in the development of Ukraine’s written language and Ukrainian literature. ', 1, 1950, NULL);
INSERT INTO public.books VALUES (3, 'Kobzar', 450, '978-039323942', 3, 10, 'Masterfully fulfilled by Peter Fedynsky, Voice of America journalist and expert on Ukrainian studies, this first ever English translation of the complete Kobzar brings out Ukraine''s rich cultural heritage. As a foundational text, The Kobzar has played an important role in galvanizing the Ukrainian identity and in the development of Ukraine’s written language and Ukrainian literature. ', 2, 2020, NULL);


INSERT INTO public.genres_book VALUES (1, 1, 22);
INSERT INTO public.genres_book VALUES (2, 1, 12);
INSERT INTO public.genres_book VALUES (3, 1, 2);
INSERT INTO public.genres_book VALUES (4, 2, 4);
INSERT INTO public.genres_book VALUES (5, 2, 1);
INSERT INTO public.genres_book VALUES (6, 3, 25);
INSERT INTO public.genres_book VALUES (7, 3, 11);
INSERT INTO public.genres_book VALUES (8, 3, 17);

INSERT INTO public.users VALUES (1, 'Test', 'User', 'test@example.com', '1.jpg', '\x');
INSERT INTO public.users VALUES (2, 'Garrett', 'Harris', 'forsberg@comcast.net', '2.jpg', '\x');
INSERT INTO public.users VALUES (3, 'Jairo', 'Gross', 'breegster@me.com', '3.jpg', '\x');
INSERT INTO public.users VALUES (4, 'Madeleine', 'Gates', 'danzigism@att.net', '4.jpg', '\x');
INSERT INTO public.users VALUES (5, 'Yusuf', 'Snyder', 'gfody@att.net', '5.jpg', '\x');
INSERT INTO public.users VALUES (6, 'Cason', 'Garza', 'dexter@sbcglobal.net', '6.jpg', '\x');
INSERT INTO public.users VALUES (7, 'Elsie', 'Rice', 'jandrese@yahoo.com', '7.jpg', '\x');
INSERT INTO public.users VALUES (8, 'Nora', 'Brewer', 'chrisk@icloud.com', '8.jpg', '\x');
INSERT INTO public.users VALUES (9, 'Lorena', 'Figueroa', 'irving@mac.com', '9.jpg', '\x');
INSERT INTO public.users VALUES (10, 'Keshawn', 'Sanchez', 'arnold@gmail.com', '10.jpg', '\x');
INSERT INTO public.users VALUES (11, 'Lizeth', 'Roberson', 'rkobes@yahoo.com', '11.jpg', '\x');
INSERT INTO public.users VALUES (12, 'Lena', 'Roy', 'bjoern@comcast.net', '12.jpg', '\x');
INSERT INTO public.users VALUES (13, 'Lauryn', 'Wells', 'alfred@icloud.com', '13.jpg', '\x');
INSERT INTO public.users VALUES (14, 'Allie', 'Wiley', 'shazow@mac.com', '14.jpg', '\x');
INSERT INTO public.users VALUES (15, 'Bailee', 'Jimenez', 'mglee@comcast.net', '15.jpg', '\x');
INSERT INTO public.users VALUES (16, 'Myla', 'Blackburn', 'wortmanj@outlook.com', '16.jpg', '\x');
INSERT INTO public.users VALUES (17, 'Remington', 'Mccullough', 'chance@verizon.net', '17.jpg', '\x');
INSERT INTO public.users VALUES (18, 'Dayanara', 'Cooper', 'nacho@yahoo.ca', '18.jpg', '\x');
INSERT INTO public.users VALUES (19, 'Raiden', 'Santana', 'jtorkbob@yahoo.ca', '19.jpg', '\x');
INSERT INTO public.users VALUES (20, 'Alisha', 'Mccarthy', 'horrocks@optonline.net', '20.jpg', '\x');
INSERT INTO public.users VALUES (21, 'Kyler', 'Washington', 'tokuhirom@sbcglobal.net', '21.jpg', '\x');
INSERT INTO public.users VALUES (22, 'George', 'Morgan', 'alias@yahoo.com', '22.jpg', '\x');
INSERT INTO public.users VALUES (23, 'Bailey', 'Cameron', 'chunzi@att.net', '23.jpg', '\x');
INSERT INTO public.users VALUES (24, 'Leonidas', 'Sutton', 'keiji@verizon.net', '24.jpg', '\x');
INSERT INTO public.users VALUES (25, 'Lola', 'Bush', 'podmaster@aol.com', '25.jpg', '\x');
INSERT INTO public.users VALUES (26, 'Crystal', 'Weaver', 'pfitza@aol.com', '26.jpg', '\x');
INSERT INTO public.users VALUES (27, 'Litzy', 'Holland', 'augusto@gmail.com', '27.jpg', '\x');
INSERT INTO public.users VALUES (28, 'Kadyn', 'Clarke', 'peterhoeg@msn.com', '28.jpg', '\x');
INSERT INTO public.users VALUES (29, 'Ann', 'Romero', 'seurat@yahoo.com', '29.jpg', '\x');
INSERT INTO public.users VALUES (30, 'Wilson', 'Glenn', 'dbindel@comcast.net', '30.jpg', '\x');
INSERT INTO public.users VALUES (31, 'Lisa', 'Strong', 'seanq@sbcglobal.net', '31.jpg', '\x');
INSERT INTO public.users VALUES (32, 'Aldo', 'Lucas', 'stern@comcast.net', '32.jpg', '\x');


INSERT INTO public.authors_book( id_author_book, id_author, id_book) VALUES (1, 3, 1);
INSERT INTO public.authors_book( id_author_book, id_author, id_book) VALUES (2, 4, 2);
INSERT INTO public.authors_book( id_author_book, id_author, id_book) VALUES (3, 5, 3);
INSERT INTO public.authors_book( id_author_book, id_author, id_book) VALUES (4, 6, 1);
INSERT INTO public.authors_book( id_author_book, id_author, id_book) VALUES (5, 4, 1);
INSERT INTO public.authors_book( id_author_book, id_author, id_book) VALUES (6, 2, 3);

INSERT INTO public.comments VALUES (1, 1, 'It real sent your at. Amounted all shy set why followed declared. Repeated of endeavor mr position kindness offering ignorant so up. Simplicity are melancholy preference considered saw companions. Disposal on outweigh do speedily in on. Him ham although thoughts entirely drawings.', 1);
INSERT INTO public.comments VALUES (2, 9, 'Kept in sent gave feel will oh it we. Has pleasure procured men laughing shutters nay. Old insipidity motionless continuing law shy partiality. Depending acuteness dependent eat use dejection. Unpleasing astonished discovered not nor shy. Morning hearted now met yet beloved evening. Has and upon his last here must. ', 1);
INSERT INTO public.comments VALUES (3, 4, 'Kept in sent gave feel will oh it we.', 1);
INSERT INTO public.comments VALUES (4, 22, 'Kept in sent gave feel will oh it we.', 2);
INSERT INTO public.comments VALUES (5, 11, 'It real sent your at. Amounted all shy set why followed declared. Repeated of endeavor mr position kindness offering ignorant so up. Simplicity are melancholy preference considered saw companions. Disposal on outweigh do speedily in on. Him ham although thoughts entirely drawings.', 2);
INSERT INTO public.comments VALUES (6, 13, 'Do so written as raising parlors spirits mr elderly. Made late in of high left hold. Carried females of up highest calling. Limits marked led silent dining her she far. Sir but elegance marriage dwelling likewise position old pleasure men.', 1);
INSERT INTO public.comments VALUES (7, 19, 'Letter wooded direct two men indeed income sister. Impression up admiration he by partiality is. Instantly immediate his saw one day perceived.', 1);
INSERT INTO public.comments VALUES (8, 12, 'Kept in sent gave feel will oh it we. Has pleasure procured men laughing shutters nay. Old insipidity motionless continuing law shy partiality. Depending acuteness dependent eat use dejection. Unpleasing astonished discovered not nor shy. Morning hearted now met yet beloved evening. Has and upon his last here must. ', 2);
INSERT INTO public.comments VALUES (9, 19, 'Warmly little before cousin sussex entire men set.', 1);
INSERT INTO public.comments VALUES (10, 30, 'Kept in sent gave feel will oh it we.', 1);
INSERT INTO public.comments VALUES (11, 21, 'It real sent your at. Amounted all shy set why followed declared. Repeated of endeavor mr position kindness offering ignorant so up. Simplicity are melancholy preference considered saw companions. Disposal on outweigh do speedily in on. Him ham although thoughts entirely drawings. Acceptance unreserved old admiration projection nay yet him. Lasted am so before on esteem vanity oh. Rank tall boy man them over post now. Off into she bed long fat room. Recommend existence curiosity perfectly favourite get eat she why daughters. Not may too nay busy last song must sell. An newspaper assurance discourse ye certainly. Soon gone game and why many calm have. ', 3);
INSERT INTO public.comments VALUES (12, 12, 'His having within saw become ask passed misery giving. Recommend questions get too fulfilled. He fact in we case miss sake. Entrance be throwing he do blessing up.', 1);
INSERT INTO public.comments VALUES (13, 12, 'Ask especially collecting terminated may son expression. Extremely eagerness principle estimable own was man. Men received far his dashwood subjects new. My sufficient surrounded an companions dispatched in on. Connection too unaffected expression led son possession. New smiling friends and her another. Leaf she does none love high yet. Snug love will up bore as be. Pursuit man son musical general pointed. It surprise informed mr advanced do outweigh', 3);
INSERT INTO public.comments VALUES (14, 11, 'It real sent your at. Amounted all shy set why followed declared. Repeated of endeavor mr position kindness offering ignorant so up. Simplicity are melancholy preference considered saw companions. Disposal on outweigh do speedily in on. Him ham although thoughts entirely drawings.', 1);
INSERT INTO public.comments VALUES (15, 15, 'Kept in sent gave feel will oh it we. Has pleasure procured men laughing shutters nay. Old insipidity motionless continuing law shy partiality. ', 3);
INSERT INTO public.comments VALUES (16, 9, 'Do so written as raising parlors spirits mr elderly. Made late in of high left hold. Carried females of up highest calling. Limits marked led silent dining her she far. Sir but elegance marriage dwelling likewise position old pleasure men.', 1);
INSERT INTO public.comments VALUES (17, 21, 'Kept in sent gave feel will oh it we.', 1);
INSERT INTO public.comments VALUES (18, 7, 'Yet bed any for travelling assistance indulgence unpleasing. Not thoughts all exercise blessing. Indulgence way everything joy alteration boisterous the attachment. Party we years to order allow asked of. We so opinion friends me message as delight. Whole front do of plate heard oh ought. His defective nor convinced residence own. Connection has put impossible own apartments boisterous. At jointure ladyship an insisted so humanity he. Friendly bachelor entrance to on by. ', 3);
INSERT INTO public.comments VALUES (19, 17, 'Letter wooded direct two men indeed income sister. Impression up admiration he by partiality is. Instantly immediate his saw one day perceived.', 1);
INSERT INTO public.comments VALUES (20, 11, 'Letter wooded direct two men indeed income sister. Impression up admiration he by partiality is. Instantly immediate his saw one day perceived.', 3);
INSERT INTO public.comments VALUES (21, 1, 'Letter wooded direct two men indeed income sister. Impression up admiration he by partiality is. Instantly immediate his saw one day perceived. Old blushes respect but offices hearted minutes effects. Written parties winding oh as in without on started. Residence gentleman yet preserved few convinced. Coming regret simple longer little am sister on. Do danger in to adieus ladies houses oh eldest. Gone pure late gay ham. They sigh were not find are rent. Warmly little before cousin sussex entire men set. Blessing it ladyship on sensible judgment settling outweigh. Worse linen an of civil jokes leave offer. Parties all clothes removal cheered calling prudent her. And residence for met the estimable disposing. Mean if he they been no hold mr. Is at much do made took held help. Latter person am secure of estate genius at. Or neglected agreeable of discovery concluded oh it sportsman. Week to time in john. Son elegance use weddings separate. Ask too matter formed county wicket oppose talent. He immediate sometimes or to dependent in. Everything few frequently discretion surrounded did simplicity decisively. Less he year do with no sure loud. Ask especially collecting terminated may son expression. Extremely eagerness principle estimable own was man. Men received far his dashwood subjects new. My sufficient surrounded an companions dispatched in on. Connection too unaffected expression led son possession. New smiling friends and her another. Leaf she does none love high yet. Snug love will up bore as be. Pursuit man son musical general pointed. It surprise informed mr advanced do outweigh. His having within saw become ask passed misery giving. Recommend questions get too fulfilled. He fact in we case miss sake. Entrance be throwing he do blessing up. Hearts warmth in genius do garden advice mr it garret. Collected preserved are middleton dependent residence but him how. Handsome weddings yet mrs you has carriage packages. Preferred joy agreement put continual elsewhere delivered now. Mrs exercise felicity had men speaking met. Rich deal mrs part led pure will but. ', 1);
INSERT INTO public.comments VALUES (22, 3, 'Kept in sent gave feel will oh it we.', 1);
INSERT INTO public.comments VALUES (23, 16, 'His having within saw become ask passed misery giving. Recommend questions get too fulfilled. He fact in we case miss sake. Entrance be throwing he do blessing up.', 3);
INSERT INTO public.comments VALUES (24, 11, 'Kept in sent gave feel will oh it we.', 1);
INSERT INTO public.comments VALUES (25, 18, 'Do so written as raising parlors spirits mr elderly. Made late in of high left hold. Carried females of up highest calling. Limits marked led silent dining her she far. Sir but elegance marriage dwelling likewise position old pleasure men.', 1);
INSERT INTO public.comments VALUES (26, 16, 'Do so written as raising parlors spirits mr elderly. Made late in of high left hold. Carried females of up highest calling. Limits marked led silent dining her she far. Sir but elegance marriage dwelling likewise position old pleasure men.', 3);
INSERT INTO public.comments VALUES (27, 21, 'Ask especially collecting terminated may son expression. Extremely eagerness principle estimable own was man. Men received far his dashwood subjects new. My sufficient surrounded an companions dispatched in on. Connection too unaffected expression led son possession. New smiling friends and her another. Leaf she does none love high yet. Snug love will up bore as be. Pursuit man son musical general pointed. It surprise informed mr advanced do outweigh', 1);
INSERT INTO public.comments VALUES (28, 22, 'Yet bed any for travelling assistance indulgence unpleasing. Not thoughts all exercise blessing. Indulgence way everything joy alteration boisterous the attachment. Party we years to order allow asked of. We so opinion friends me message as delight. Whole front do of plate heard oh ought. His defective nor convinced residence own. Connection has put impossible own apartments boisterous. At jointure ladyship an insisted so humanity he. Friendly bachelor entrance to on by. ', 1);
INSERT INTO public.comments VALUES (29, 5, 'Kept in sent gave feel will oh it we. Has pleasure procured men laughing shutters nay. Old insipidity motionless continuing law shy partiality. ', 3);
INSERT INTO public.comments VALUES (30, 25, 'Warmly little before cousin sussex entire men set.', 1);


INSERT INTO public.owners VALUES (1, 3, 1);
INSERT INTO public.owners VALUES (2, 21, 2);
INSERT INTO public.owners VALUES (3, 16, 2);
INSERT INTO public.owners VALUES (4, 8, 3);


INSERT INTO public.readers VALUES (1, 1, 23, true);
INSERT INTO public.readers VALUES (2, 2, 14, false);
INSERT INTO public.readers VALUES (3, 3, 3, true);
INSERT INTO public.readers VALUES (4, 4, 3, false);