const dbFunction = require('./db-function');
const db = dbFunction.connectDb();

const validator = require('validator');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

// Get all athors for /authors request
function getAuthors(req, res, next) {
  db.any('SELECT * FROM authors')
    .then((data) => {
      res.status(200)
        .json({ status: 'success', data: data });
    })
    .catch((err) => {
      return next(err);
    });
}

// Get single author with id for /author/:id request
function getAuthorById(req, res, next) {
  const idAuthor = parseInt(req.params.id);
  db.one("SELECT * FROM authors WHERE id_author = $1", idAuthor)
    .then((data) => {
      res.status(200) 
      .json({ data });
    })
    .catch((err) => {
      return next(err);
    });
}

// Get comments with book id for /comments/:id request
function getCommentsByBookId(req, res, next) {
  const idBook = parseInt(req.params.id);
  db.any(`SELECT id_book, id_user, id_book, comment, 
                  (SELECT name_user FROM users WHERE comments.id_user = users.id_user),
                  (SELECT surname_user FROM users WHERE comments.id_user = users.id_user)
          FROM comments
          WHERE id_book = $1`, idBook)
    .then((data) => {
      res.status(200)
      .json({ data });
    })
    .catch((err) => {
      return next(err);
    });
}

// Get all books for /books/ request
function getBooks(req, res, next) {
  db.any(`SELECT *, languages.name_language FROM books
          INNER JOIN languages ON books.id_language = languages.id_language
          INNER JOIN publishers ON books.id_publisher = publishers.id_publisher`)
    .then((data) => {
      res.status(200)
      .json({ data });
    })
    .catch((err) => {
      return next(err);
    });
}


// Get book with id for /book/:id request
function getBooksById(req, res, next) {
  const idBook = parseInt(req.params.id);
  db.one(`SELECT *, languages.name_language FROM books
        INNER JOIN languages ON books.id_language = languages.id_language
        INNER JOIN publishers ON books.id_publisher = publishers.id_publisher
        WHERE id_book = $1`, idBook)
    .then((data) => {
      res.status(200)
      .json({ data });
    })
    .catch((err) => {
      return next(err);
    });
}

// Get authors of book with book id for /authorsbook/:id request
function getAuthorsByBookId(req, res, next) {
  const idBook = parseInt(req.params.id);
  db.any("SELECT * FROM authors WHERE id_author IN (SELECT id_author FROM authors_book WHERE id_book = $1)", idBook)
    .then((data) => {
      res.status(200)
      .json({ data });
    })
    .catch((err) => {
      return next(err);
    });
}

// Get books of author with author id for /booksauthor/:id request
function getBooksByAuthorId(req, res, next) {
  const idAuthor = parseInt(req.params.id);
  db.any("SELECT * FROM books WHERE id_book IN (SELECT id_book FROM authors_book WHERE id_author = $1)", idAuthor)
    .then((data) => {
      res.status(200)
      .json({ data });
    })
    .catch((err) => {
      return next(err);
    });
}


// Get all genres  for /genres request
function getGenres(req, res, next) {
  db.any("SELECT * FROM genres")
    .then((data) => {
      res.status(200)
      .json({ data });
    })
    .catch((err) => {
      return next(err);
    });
}

// Get genres by book id for /genres/book/:id request
function getGenresByBookID(req, res, next) {
  const idBook = parseInt(req.params.id);
  db.any("SELECT * FROM genres WHERE id_genre IN (SELECT id_genre FROM genres_book WHERE id_book = $1)", idBook)
    .then((data) => {
      res.status(200)
      .json({ data });
    })
    .catch((err) => {
      return next(err);
    });
}

// vGet books by genres id for /books/genre/:id request
function getBooksByGenreID(req, res, next) {
  const idGenre = parseInt(req.params.id);
  db.any("SELECT * FROM books WHERE id_book IN (SELECT id_book FROM genres_book WHERE id_genre = $1)", idGenre)
    .then((data) => {
      res.status(200)
      .json({ data });
    })
    .catch((err) => {
      return next(err);
    });
}

// Get all languages for /languages request
function getLanguages(req, res, next) {
  db.any("SELECT * FROM languages")
    .then((data) => {
      res.status(200)
      .json({ data });
    })
    .catch((err) => {
      return next(err);
    });
}

//Get lenguages by id for /language/:id request
function getLanguageByID(req, res, next) {
  const idLanguage = parseInt(req.params.id);
  db.one("SELECT * FROM languages WHERE id_language = $1", idLanguage)
    .then((data) => {
      res.status(200)
      .json({ data });
    })
    .catch((err) => {
      return next(err);
    });
}


// Get owners by book id for /owners/book/:id request
function getOwnersByBookID(req, res, next) {
  const idOwner = parseInt(req.params.id);
  db.any("SELECT * FROM users WHERE id_user IN (SELECT id_user FROM owners WHERE id_book = $1)", idOwner)
    .then((data) => {
      res.status(200)
      .json({ data });
    })
    .catch((err) => {
      return next(err);
    });
}


// Get all publisher for /publishers request
function getPublishers(req, res, next) {
  db.any("SELECT * FROM publishers")
    .then((data) => {
      res.status(200)
      .json({ data });
    })
    .catch((err) => {
      return next(err);
    });
}


//Get publisher by id for /publishers/:id request
function getPublisherByID(req, res, next) {
  const idPublisher = parseInt(req.params.id);
  db.one("SELECT * FROM publishers WHERE id_publisher = $1", idPublisher)
    .then((data) => {
      res.status(200)
      .json({ data });
    })
    .catch((err) => {
      return next(err);
    });
}



// Get reders by book id for /reader/book/:id request
function getReadersByBookID(req, res, next) {
  const idOwner = parseInt(req.params.id);
  db.any("SELECT * FROM users WHERE id_user IN (SELECT id_user FROM readers WHERE id_owner IN (SELECT id_owner FROM owners WHERE id_book = $1))", idOwner)
    .then((data) => {
      res.status(200)
      .json({ data });
    })
    .catch((err) => {
      return next(err);
    });
}


// Get user by id for /user/:id request
function getUserByID(req, res, next) {
  const idUser = parseInt(req.params.id);
  db.one("SELECT * FROM users WHERE id_user = $1", idUser)
    .then((data) => {
      res.status(200)
      .json({ data });
    })
    .catch((err) => {
      return next(err);
    });
}


// Get user by id email /user/email/:email request
function getUserByEmail(req, res, next) {
  const email = req.params.email.toString();
  const query = `SELECT * FROM users WHERE email = \'${email}\'`;
  console.log('-----------------  - '+ query);
  db.one(query)
    .then((data) => {
      res.status(200)
      .json({ data });
    })
    .catch((err) => {
      return next(err);
    });
}

//----------------------------------------------------------------------------------
// put functions

function addBook(req, res, next) {
  const name_book = req.body.name_book;
  const count_of_pages = parseInt(req.body.count_of_pages);
  const isbn = req.body.isbn;
  const id_language = parseInt(req.body.id_language);
  const id_publisher = parseInt(req.body.id_publisher);
  const description = req.body.description;
  const mark = parseInt(req.body.mark);
  const year_publish = parseInt(req.body.year_publish);

  db.none(`INSERT INTO books(name_book, count_of_pages, isbn, id_language, id_publisher, description, mark, year_publish)  
   VALUES ('${name_book}', ${count_of_pages}, ${isbn}, ${id_language}, ${id_publisher}, '${description}', ${mark}, ${year_publish})`)
    .then(function () {
      res.status(200)
        .json({
          status: 'success',
          message: 'Inserted one book'
        });
    })
    .catch(function (err) {
      console.log(err);
      return next(err);
    });
}

// ------------------------------------------------------------------------------
function addComment(req, res, next) {

  const comment = req.body.comment;
  const id_user = parseInt(req.body.id_user);
  const id_book = parseInt(req.body.id_book);

  const sqls = `INSERT INTO comments(id_user, id_book, comment) VALUES (${id_user}, ${id_book}, '${comment}')`;

  console.log(sqls);
  db.none(sqls)
    .then(function () {
      res.status(200)
        .json({
          status: 'success',
          message: 'Inserted one book'
        });
    })
    .catch(function (err) {
      console.log(err);
      return next(err);
    });
}













//---------------------------------------------------------------------------------

// Registration Validation
// All fields Can't be blank
// Password must be more that 10 chars
// Email must be email type
function validUser(user){
  const validEmail = typeof user.userEmail === 'string' &&
    user.userEmail.trim() !== '' &&
    validator.isEmail(user.userEmail);
  const validPassword = typeof user.userPassword === 'string' &&
    user.userPassword.trim() !== '' &&
    user.userPassword.trim().length >= 6;
  const validName = typeof user.userName === 'string' &&
                           user.userName.trim() !== '';
  const validSurname = typeof user.userSurname === 'string' &&
                       user.userSurname.trim() !== '';

  return validEmail && validPassword && validName && validSurname;
}

function validEmailAndPassword(user){
  const validEmail = typeof user.userEmail === 'string' &&
    user.userEmail.trim() !== '' &&
    validator.isEmail(user.userEmail);
  const validPassword = typeof user.userPassword === 'string' &&
    user.userPassword.trim() !== '' &&
    user.userPassword.trim().length >= 6;

  return validEmail && validPassword;
}

// Add new user to DataBase POST
function postValidUser(req, res, next) {
  const userName = req.body.userName;
  const userSurname = req.body.userSurname;
  const userEmail = req.body.userEmail;
  const userPassword = req.body.userPassword;
  if (validUser(req.body)){
    db.any("SELECT * FROM users WHERE email = $1", userEmail)
      .then((data) => {
        if ( data.length === 0 ){
          bcrypt.hash(userPassword, 10)
            .then((hash) => {
                db.none('INSERT INTO users(name_user, surname_user, email, password) VALUES ($1, $2, $3, $4)'
                  ,[userName, userSurname, userEmail, hash])
                  .then(() => {
                    return res.status(200).send({message: "New user have been created"});
                  })
                  .catch(function (err) {
                    return next(err);
                  });
          });
        }else{
          return res.status(400).send({message: "Email already exist"});
        }
        })
      .catch((err) => {
        return next(err);
      });
  }else{
    return res.status(400).send({message: "Invalid User"});
  }
}

// Login user
function userLogin(req, res, next) {
  const userEmail = req.body.userEmail;
  const userPassword = req.body.userPassword;
  if (validEmailAndPassword(req.body)){
    db.any("SELECT * FROM users WHERE email = $1", userEmail)
      .then((data) => {
        if ( data.length !== 0 ){
          bcrypt.compare(userPassword, data[0].password)
            .then((result) => {
              console.log('we are here!');

              if (result){
                let token = jwt.sign({ email: userEmail }, 'secret');
                return res.status(200).send({
                  message: "Logged In",
                  token: token,
                });
              }else{
                return res.status(400).send({message: "Passwords doesn't match"});
              }
            });
        }else{
          return res.status(400).send({message: "Email not exist"});
        }
      })
      .catch((err) => {
        return next(err);
      });
  }else{
    return res.status(400).send({message: "Invalid User"});
  }
}

// Get User Information byt Token
function getUserByToken(req, res, next) {
  const token = req.body.token;
  var email = jwt.verify(token, 'secret');
  const query = `SELECT * FROM users WHERE email = \'${email}\'`;
  db.one(query)
    .then((data) => {
      return res.status(200).send(data);
    })
    .catch((err) => {
      return next(err);
    });
}


module.exports = {
  getAuthors: getAuthors,
  getAuthorById: getAuthorById,
  getCommentsByBookId: getCommentsByBookId,
  getBooks: getBooks,
  getBooksById: getBooksById,
  getAuthorsByBookId: getAuthorsByBookId,
  getBooksByAuthorId: getBooksByAuthorId,
  getGenres: getGenres,
  getGenresByBookID: getGenresByBookID,
  getBooksByGenreID: getBooksByGenreID,
  getLanguages: getLanguages,
  getLanguageByID: getLanguageByID,
  getOwnersByBookID: getOwnersByBookID,
  getPublishers: getPublishers,
  getPublisherByID: getPublisherByID,
  getReadersByBookID: getReadersByBookID,
  getUserByID: getUserByID,
  getUserByEmail: getUserByEmail,

  postValidUser: postValidUser,
  userLogin: userLogin,
  getUserByToken: getUserByToken,
  addBook: addBook,
  addComment: addComment,
};