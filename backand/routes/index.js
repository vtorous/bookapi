var express = require('express');
var router = express.Router();
var get_queries = require('../db/get-queries');


router.get('/authors', get_queries.getAuthors);
router.get('/author/:id', get_queries.getAuthorById);
router.get('/authors/book/:id', get_queries.getAuthorsByBookId);

router.get('/comments/:id', get_queries.getCommentsByBookId);

router.get('/books', get_queries.getBooks);
router.get('/book/:id', get_queries.getBooksById);
router.get('/books/author/:id', get_queries.getBooksByAuthorId);
router.get('/books/genre/:id', get_queries.getBooksByGenreID);

router.get('/genres', get_queries.getGenres);
router.get('/genres/book/:id', get_queries.getGenresByBookID);

router.get('/languages', get_queries.getLanguages);
router.get('/language/:id', get_queries.getLanguageByID);

router.get('/owners/book/:id', get_queries.getOwnersByBookID);

router.get('/publishers', get_queries.getPublishers);
router.get('/publisher/:id', get_queries.getPublisherByID);

router.get('/readers/book/:id', get_queries.getReadersByBookID);

router.get('/user/:id', get_queries.getUserByID);
router.get('/user/email/:email', get_queries.getUserByEmail);

router.post('/api/add-book/', get_queries.addBook);
router.post('/api/add-comment/', get_queries.addComment);


router.post('/api/auth', get_queries.postValidUser);
router.post('/api/login', get_queries.userLogin);


module.exports = router;
